package clases;

/**
 * Esta clase se crea al solo efecto de proveer un metodo main() de prueba.
 *
 * @author Ing. Valerio Frittelli.
 * @version Agosto de 2017.
 */
public class Principal 
{
    /**
     * Metodo de entrada del programa.
     * @param args parametros para la linea de órdenes.
     */
    public static void main(String args[]) 
    {
        Persona a = new Persona("Juan", 25);
        System.out.print("Datos de la Persona: ");
        System.out.println(a);
    }
}
