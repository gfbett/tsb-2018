package clases;

/**
 * Un vector generico basico.
 * 
 * @author Ing. Valerio Frittelli.
 * @version Mayo de 2004.
 */
import javax.swing.JOptionPane;
public final class Listado
{
   private Comparable v[]; 
   private int largo;       
   private int libre;       
  
   /**
    *  Se crea un Listado de 10 elementos de largo, con referencias nulas.
    */
   public Listado()
   {
      this(10);
   }
   
   /**
    * Crea un Listado de n elementos de largo, con referencias nulas, siempre 
    * y cuando n sea mayor a cero. De lo contrario, el Listado se crea de 
    * capacidad 10.
    * @param n la capacidad del listado a crear.
    */
   public Listado(int n)
   {
      if (n <= 0) { n = 10; }
      largo = n;
      libre = 0;
      v = new Comparable[largo];
   }
   
   /**
    *  Metodo de acceso para la capacidad del Listado. Provisto en lugar 
    *  de "getLargo" por razones de claridad.
    *  @return la capacidad del Listado.
    */
   public int length()
   {
      return largo;   
   }
   
   /**
    *  Metodo de acceso al indice de la primera casilla libre en el Listado. 
    *  @return la capacidad del Listado.
    */
   public int getPrimeraLibre()
   {
      return libre;   
   }
      
   @Override
   public String toString()
   {
      StringBuilder cad = new StringBuilder("Contenido:\n");
      int i;
      for(i=0; i<libre; i++)
      {
          cad.append(v[i].toString()).append("; ");   
      }
      return cad.toString();
   }
   
   /**
    * Agrega un elemento al Listado, en la primera casilla libre. El metodo supone que 
    * el atributo "libre" indica el indice de la primera casilla que esta desocupada en 
    * el arreglo, completandose de izquierda a derecha.
    * @param x el objeto Comparable a agregar al Listado.
    */
   public void add (Comparable x)
   {
      //boolean res = false;
      if (libre == 0 || (libre > 0 && libre < largo && x.getClass() == v[0].getClass())) 
      {
         v[libre] = x;
         libre++;
         //res = true;
      }
      else
      {
         NullPointerException y = new NullPointerException("Prohibido");
         throw y;
      }
      //return res;
   }
   
   /**
    * Devuelve una referencia al objeto ubicado en la posicion i del Listado, sin 
    * removerlo del mismo.
    * @param i el indice de la casilla con el objeto a retornar.
    * @return la referencia al objeto, o null si el indice estaba fuera de rango.
    */
   public Comparable get (int i)
   {
     Comparable x = null;
     if (i < libre)
     {
          try
          {
             x = v[i];
          }
          catch(ArrayIndexOutOfBoundsException ex)
          {
              // si llego aca, el indice "i" era negativo... Mostramos un mensaje y retornamos null...
              JOptionPane.showMessageDialog(null, "Indice fuera de rango", "Error...", JOptionPane.ERROR_MESSAGE); 
          }
     }
     return x;
   }

   /**
    * Busca un objeto en el Listado.
    * @param x el objeto a buscar.
    * @return el indice la casilla que contiene a x, si existe. Si x no existe en el Listado, retorna -1.
    */
   public int buscar (Comparable x)
   {
     int i, ind = -1;
     for (i=0; i<libre; i++)
     {
        if (x.compareTo(v[i]) == 0)
        {
           ind = i;
           break;   
        }
     }
     return ind;
   }

   /** 
    * Ordena el Listado con el metodo Quick Sort.
    */
   public void ordenar ()
   {
      quick (0, libre - 1);
   }

   private void quick (int izq, int der)
   {
       Comparable x, y;
       int i, j;
       i = izq;
       j = der;
       x = v[(izq + der) / 2];
       do 
       {
            while (v[i].compareTo(x) < 0 && i < der)
            {
               i++;
            }
            while (x.compareTo(v[j]) < 0 && j > izq)
            {
               j--;
            }
            if (i<=j)
            {
                  y = v[i];
                  v[i] = v[j];
                  v[j] = y;
                  i++;
                  j--;
            }
       }
       while (i <= j);
       if (izq < j) { quick (izq, j); }
       if (i < der) { quick (i, der); }
   }
}
