package clases;

import java.util.Scanner;

/**
 *  Provee el metodo main para la carga de datos en un Listado. 
 *  @author Ing. Valerio Frittelli.
 *  @version Agosto de 2013.
 */
public class Principal
{
    private static Scanner sc = new Scanner(System.in);    
    private static Listado lis;
    
    public static void main(String args[])
    {       
        System.out.print("Ingrese cantidad de elementos: ");
        int n = sc.nextInt();
        lis = new Listado(n);
        
        int op;
        do
        {
            System.out.println("1. Insertar un numero en el vector");
            System.out.println("2. Mostrar el vector");
            System.out.println("3. Acceder a un casillero");
            System.out.println("4. Salir");
            System.out.print("\t\tIngrese opcion: ");
            op = sc.nextInt();
            switch(op)
            {
                case 1: cargar();
                        break;
                
                case 2: mostrar();
                        break;
                
                case 3: acceder();
                        break;
                        
                case 4: ;
            }
        }
        while(op != 4);
    }
    
    public static void cargar()
    {
        /*
        System.out.print("Ingrese un numero para agregar al vector: ");
        int num = Consola.readInt();
        lis.add(num);
        //*/
        
        //*
        try
        {
           System.out.print("Ingrese un numero para agregar al vector: ");
           int num = sc.nextInt();
           //int num = Integer.parseInt(Consola.readLine());
           lis.add(num);
           
           //return;
           
           //System.exit(1);
        }
        catch(NumberFormatException e)
        {
           System.out.println("Formato de numero incorrecto...");  
           
           //return;
           
           //System.exit(1);
        }
        
        finally
        {
           System.out.println("Se ejecuto el finally!!!");
        }
        //*/
        
        System.out.println();
    }
    
    public static void mostrar()
    {
        System.out.println("Contenido del vector:");
        System.out.println(lis.toString());
        System.out.println();        
    }
    
    public static void acceder()
    {
        System.out.print("Indice del casillero a acceder: ");
        int i = sc.nextInt();
        System.out.println("Valor accedido: " + lis.get(i));
        System.out.println();
    }
}
