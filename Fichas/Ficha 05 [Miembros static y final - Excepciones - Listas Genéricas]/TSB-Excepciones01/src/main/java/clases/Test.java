package clases;
import java.io.*;

public class Test 
{
    public static void main(String args[]) // throws FileNotFoundException, IOException
    {
      RandomAccessFile m;
      int v[] = new int[5];
      try
      {
          v[3] = 4;
          v[4] = 7;
       
          m = new RandomAccessFile("prueba.dat", "r");
          m.write(200);

       }
       catch(ArrayIndexOutOfBoundsException e)
       {
           System.out.println(e.getMessage());
       }
       
       catch(FileNotFoundException e)
       {
            System.out.println(e.getMessage());
       }
        
       catch(IOException e)
       {
            System.out.println(e.getMessage());
       }
       
       System.out.println("Ok...");
    }
}
