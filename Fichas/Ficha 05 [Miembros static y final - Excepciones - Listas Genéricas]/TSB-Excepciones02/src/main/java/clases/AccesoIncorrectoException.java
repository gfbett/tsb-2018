package clases;

/**
 * Describe un intento de acceder a objetos aun no creados en el Listado.
 * 
 * @author Ing. Valerio Frittelli.
 * @version Mayo de 2004.
 */
public class AccesoIncorrectoException extends Exception
{
    private String mensaje = "Error: Acceso Incorrecto";
    
	/**
	 * Constructor default.
	 */
	public AccesoIncorrectoException()
	{
	}

	/**
	 * Constructor. Toma el mensaje como parametro.
	 */
	public AccesoIncorrectoException(String mens)
	{
	    mensaje = mens;
	}
	
	/**
	 * Redefinicion del metodo heredado desde Throwable.
	 * @return un String con la descripcion del error.
	 */
        @Override
	public String getMessage()
	{
	   return mensaje;   
	}
}
