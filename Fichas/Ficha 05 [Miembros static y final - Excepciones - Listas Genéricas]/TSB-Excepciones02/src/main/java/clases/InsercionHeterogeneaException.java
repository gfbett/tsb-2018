package clases;

/**
 * Describe un intento de insertar objetos de tipos distintos en una estructura homogenea.
 * 
 * @author Ing. Valerio Frittelli.
 * @version Mayo de 2004.
 */
public class InsercionHeterogeneaException extends Exception
{
    private String mensaje = "Error: Insercion Heterogenea";
    
	/**
	 * Constructor por fefecto.
	 */
	public InsercionHeterogeneaException()
	{
	}

	/**
	 * Constructor. Toma el mensaje como parametro.
	 */
	public InsercionHeterogeneaException(String mens)
	{
	    mensaje = mens;
	}
	
	/**
	 * Redefinicion del metodo heredado desde Throwable.
	 * @return un String con la descripcion del error.
	 */
        @Override
	public String getMessage()
	{
	   return mensaje;   
	}
}
