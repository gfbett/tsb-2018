package clases;

public final class Listado
{
   private Comparable v[]; 
   private int largo;       
   private int libre;       
  
   public Listado()
   {
      this(10);
   }
   
   public Listado(int n)
   {
      if (n <= 0) { n = 10; }
      largo = n;
      libre = 0;
      v = new Comparable[largo];
   }
   
   public int length()
   {
      return largo;   
   }
   
   public int getPrimeraLibre()
   {
      return libre;   
   }
      
   @Override
   public String toString()
   {
      StringBuilder cad = new StringBuilder("Contenido:\n");
      int i;
      for(i=0; i<libre; i++)
      {
          cad.append(v[i].toString()).append("; ");   
      }
      return cad.toString();
   }
   
   public boolean add (Comparable x) throws InsercionHeterogeneaException
   {
      if (libre > 0 && x.getClass() != v[0].getClass()) 
      { throw new InsercionHeterogeneaException("Oiga que hace...#@##@@#$$$#@#$#@!!!!"); }
      
      boolean res = false;
      if (libre < largo) 
      {
         v[libre] = x;
         libre++;
         res = true;
      }
      return res;
   }
   
   public Comparable get (int i) throws AccesoIncorrectoException
   {
      Comparable x = null;
      if (i >= libre) { throw new AccesoIncorrectoException("No existe el objeto en la posicion " + i); }
      try
      {
         x = v[i];
      }
      catch(IndexOutOfBoundsException ex)
      {
          javax.swing.JOptionPane.showMessageDialog(null, "Indice negativo o fuera de rango: verifique!!!"); 
      }
      return x;
   }

   public int buscar (Comparable x)
   {
     int i, ind = -1;
     for (i=0; i<libre; i++)
     {
        if (x.compareTo(v[i]) == 0)
        {
           ind = i;
           break;   
        }
     }
     return ind;
   }

   public void ordenar ()
   {
      quick (0, libre - 1);
   }

   private void quick (int izq, int der)
   {
       Comparable x, y;
       int i, j;
       i = izq;
       j = der;
       x = v[(izq + der) / 2];
       do 
       {
            while (v[i].compareTo(x) < 0 && i < der) {
               i++;
            }
            while (x.compareTo(v[j]) < 0 && j > izq) {
               j--;
            }
            if (i<=j)
            {
                  y = v[i];
                  v[i] = v[j];
                  v[j] = y;
                  i++;
                  j--;
            }
       }
       while (i <= j);
       if (izq < j) { quick (izq, j); }
       if (i < der) { quick (i, der); }
   }
}
