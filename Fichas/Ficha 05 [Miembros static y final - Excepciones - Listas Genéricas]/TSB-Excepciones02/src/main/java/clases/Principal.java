package clases;

import java.util.Scanner;
import javax.swing.JOptionPane;
public class Principal
{
    private static Scanner sc = new Scanner(System.in);
    private static Listado lis;
    
    public static void main(String args[]) throws InsercionHeterogeneaException
    {
        System.out.print("Capacidad inicial del vector?: ");
        int n = sc.nextInt();
        lis = new Listado(n);
        
        int op;
        do
        {
            System.out.println("1. Insertar un numero en el vector");
            System.out.println("2. Mostrar el vector");
            System.out.println("3. Acceder a un casillero");
            System.out.println("4. Insercion heterogenea");
            System.out.println("5. Salir");
            System.out.print("\t\tIngrese opcion: ");
            op = sc.nextInt();
            switch(op)
            {
                case 1: cargar();
                        break;
                
                case 2: mostrar();
                        break;
                
                case 3: acceder();
                        break;
                        
                case 4: insercionIncorrecta();
                        break;
                        
                case 5: ;
            }
        }
        while(op != 5);
    }
    
    public static void cargar() // throws InsercionHeterogeneaException
    {
        /*
        System.out.print("Ingrese un numero para agregar al vector: ");
        int num = sc.nextInt();
        lis.add(8);
        lis.add("Viva Talleres");
        //*/
        
        //*
        try
        {
           System.out.print("Ingrese un numero para agregar al vector: ");
           int num = sc.nextInt();
           lis.add(num);
        }
        
        catch(InsercionHeterogeneaException ex)
        {
           JOptionPane.showMessageDialog(null, ex.getMessage());   
        }
        //*/
                
        System.out.println();
    }
    
    public static void mostrar()
    {
        System.out.println("Contenido del vector:");
        System.out.println(lis.toString());
        System.out.println();        
    }
    
    public static void acceder()
    {
        try
        {
            System.out.print("Indice del casillero a acceder: ");
            int i = sc.nextInt();
            System.out.println("Valor accedido: " + lis.get(i));
            System.out.println();
        }
        
        catch(AccesoIncorrectoException e)
        {
            JOptionPane.showMessageDialog(null, "Indice fuera de rango...");            
        }
    }
    
    public static void insercionIncorrecta()
    {
        // solo para probar...
        try
        {
            System.out.print("Ingrese un numero entero: ");
            int x = sc.nextInt();
            lis.add(x);
            System.out.println("Insercion del numero en el vector... ok...");

            System.out.print("Ingrese una cadena: ");
            String y = sc.nextLine();
	    lis.add(y);
	    System.out.println("Insercion de la cadena en el vector... ");
	   }
	   
	   catch(InsercionHeterogeneaException e)
	   {
	       JOptionPane.showMessageDialog(null, e.getMessage());
	   }
    }
}

