package clases;

public class Cuenta
{
    private int numero;
    private float saldo;
    
    /**
     * Constructor por defecto o nulo. Al incluirlo, facilitamos el proceso de creaci�n de instancias, como se ver� 
     */
    public Cuenta()
    {
    }

    public Cuenta(int num, float sal)
    {
      numero = num;
      saldo  = sal;
    }
    
    public int getNumero()
    {
       return numero;   
    }

    public float getSaldo()
    {
       return saldo;   
    }
    
    public void setNumero( int num )
    {
       numero = num;   
    }

    public void setSaldo( float sal )
    {
       saldo = sal;   
    }

    public void retirar (float imp)
    {
      if (saldo - imp >= 0)
      {
         saldo -= imp;   
      }
    }
    
    public void depositar (float imp)
    {
         saldo += imp;   
    }

    @Override
    public String toString()
    {
       return "Cuenta numero: " + numero +  " - Saldo: " + saldo;
    }
}