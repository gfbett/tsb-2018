package clases;

public class TSBNode
{
   private Object info;
   private TSBNode next;
   
   public TSBNode ( )
   {
   }
   
   public TSBNode (Object x, TSBNode p)
   {
     info = x;
     next = p;
   }
   
   public TSBNode getNext()
   {
     return next;
   }
   
   public void setNext(TSBNode p)
   {
     next = p;
   }
   
   public Object getInfo()
   {
     return info;
   }
   
   public void setInfo(Object p)
   {
     info = p;
   }

   @Override
   public String toString()
   {
     return info.toString();   
   }
}
