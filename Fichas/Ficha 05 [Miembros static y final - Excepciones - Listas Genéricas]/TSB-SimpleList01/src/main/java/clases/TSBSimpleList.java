package clases;

import java.util.NoSuchElementException;
public class TSBSimpleList
{
      private TSBNode frente;

      public TSBSimpleList ()
      {
          frente = null;
      }
      
      public void addFirst(Object x)
      {
            if(!this.isHomogeneus(x)) { return; }
            
            TSBNode p = new TSBNode(x, frente);
            frente = p;
      }  

      public void clear( )
      {
         frente = null;
      }
      
      public Object getFirst()
      {
         if (frente == null) throw new NoSuchElementException("Error: lista vacía...");
         return frente.getInfo();
      }
      
      public Object removeFirst()
      {
         if (frente == null) throw new NoSuchElementException("Error: lista vacía...");
         Object x = frente.getInfo();
         frente = frente.getNext();
         return x;
      }

      @Override
      public String toString()
      {
             TSBNode p = frente;
             String res = "[ ";
             while( p != null )
             {
                res = res + p.toString();
                if ( p.getNext() != null ) res = res + " - ";
                p = p.getNext();
             }
             res = res + " ]";
             return res;
      }
      
      private boolean isHomogeneus (Object x)
      {
            if( x == null ) { return false; }
            if(frente != null && x.getClass() != frente.getInfo().getClass())
            { 
                return false; 
            }
            return true;
      }
}
