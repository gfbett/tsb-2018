package clases;

import java.util.NoSuchElementException;
public class TSBSimpleList
{
      private TSBNode frente;
      
      public TSBSimpleList ()
      {
          frente = null;
      }
      
      public void addFirst(Comparable x)
      {
            if ( x != null )
            {
               TSBNode p = new TSBNode(x, frente);
               frente = p;
            }
      }  
      
      public void clear( )
      {
         frente = null; // �alguna duda?
      }
      
      public Comparable getFirst()
      {
         if (frente == null) throw new NoSuchElementException("Error: lista vacía...");
         
         return frente.getInfo();
      }
      
      public Comparable removeFirst()
      {
         if (frente == null) throw new NoSuchElementException("Error: lista vacía...");
         
         Comparable x = frente.getInfo();
         frente = frente.getNext();
         return x;
      }
      
      public boolean contains (Comparable x)
      {
          if (x == null) return false;
          
          TSBNode p = frente;
          while ( p != null && x.compareTo( p.getInfo() ) != 0 )
          {
                p = p.getNext();    
          }
          return ( p != null );
      }
     
      @Override
      public String toString()
      {
             TSBNode p = frente;
             String res = "[ ";
             while( p != null )
             {
                res = res + p.toString();
                if ( p.getNext() != null ) res = res + " - ";
                p = p.getNext();
             }
             res = res + " ]";
             return res;
      }
}
