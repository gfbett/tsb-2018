package clases;

import java.util.NoSuchElementException;
public class TSBSimpleList
{
    private TSBNode frente;
    private TSBNode actual; 

    public TSBSimpleList ()
    {
        frente = null;
        actual = null;
    }

    public void addFirst(Comparable x)
    {
          if ( ! isHomogeneus( x ) ) return;

          TSBNode p = new TSBNode(x, frente);
          frente = p;
    }  

    public void addInOrder(Comparable x)
    {
          if ( ! isHomogeneus( x ) ) return;

          TSBNode nuevo = new TSBNode( x, null );
          TSBNode p = frente, q = null;
          while ( p != null && x.compareTo( p.getInfo() ) >= 0 )
          {
              q = p;
              p = p.getNext();
          }
          nuevo.setNext( p );
          if( q != null ) q.setNext( nuevo );
          else frente = nuevo;
    }  

    public void addLast(Comparable x)
    {
          if ( ! isHomogeneus( x ) ) return;

          TSBNode nuevo = new TSBNode( x, null );
          TSBNode p = frente, q = null;
          while ( p != null )
          {
              q = p;
              p = p.getNext();
          }
          if( q != null ) q.setNext( nuevo );
          else frente = nuevo;
    }  

    public void clear( )
    {
       frente = null; 
       actual = null;
    }

    public boolean contains (Comparable x)
    {
        if ( ! isHomogeneus( x ) ) return false;

        TSBNode p = frente;
        while ( p != null && x.compareTo( p.getInfo() ) != 0 )
        {
              p = p.getNext();    
        }
        return ( p != null );
    }

    public Comparable getFirst()
    {
       if (frente == null) throw new NoSuchElementException("Error: la lista esta vacia...");

       return frente.getInfo();
    }

    public Comparable getLast()
    {
       if (frente == null) throw new NoSuchElementException("Error: la lista esta vacia...");

       TSBNode p = frente, q = null;
       while( p != null )
       {
          q = p;
          p = p.getNext();
       }
       return ( q != null )? q.getInfo() : frente.getInfo();
    }

    public boolean hasNext()
    {
       if ( frente == null ) { return false; }
       if ( actual != null && actual.getNext() == null ) {return false; }
       return true;
    }

    public boolean isEmpty()
    {
       return (frente == null);    
    }

    public Comparable next()
    {
        if ( ! hasNext() ) { throw new NoSuchElementException("No quedan elementos por recorrer"); }

        if ( actual == null ) { actual = frente; }
        else { actual = actual.getNext(); }
        return actual.getInfo();
    }

    public Comparable removeLast()
    {
       if (frente == null) { throw new NoSuchElementException("Error: la lista esta vacia..."); }

       TSBNode p = frente, q = null;
       while( p.getNext() != null )
       {
          q = p;
          p = p.getNext();
       }
       Comparable x = p.getInfo();
       if( q != null ) q.setNext( p.getNext() );
       else frente = p.getNext();
       return x;
    }

    public Comparable removeFirst()
    {
       if (frente == null) throw new NoSuchElementException("Error: la lista esta vacia...");

       Comparable x = frente.getInfo();
       frente = frente.getNext();
       return x;
    }

    public Comparable search (Comparable x)
    {
          if ( ! isHomogeneus( x ) )  return null;

          Comparable r = null;
          TSBNode p = frente;
          while ( p != null && x.compareTo( p.getInfo() ) != 0)
          {
              p = p.getNext();   
          }
          if ( p != null ) r = p.getInfo();
          return r;
    }

    public void startIterator()
    {
          actual = null;    
    }

    @Override
    public String toString()
    {
           TSBNode p = frente;
           String res = "[ ";
           while( p != null )
           {
              res = res + p.toString();
              if ( p.getNext() != null ) res = res + " - ";
              p = p.getNext();
           }
           res = res + " ]";
           return res;
    }

    private boolean isHomogeneus (Comparable x)
    {
          if ( x == null ) return false;
          if ( frente != null && x.getClass() != frente.getInfo().getClass() ) return false;
          return true;
    }
}
