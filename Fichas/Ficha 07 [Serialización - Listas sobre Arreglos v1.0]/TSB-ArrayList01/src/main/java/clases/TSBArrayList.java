package clases;

import java.io.Serializable;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Una clase para emular el concepto de lista implementada sobre un arreglo, tal
 * como la clase java.util.ArrayList de Java (y al estilo de la clase List de 
 * Python). Se aplícó una estrategia de desarrollo basada en emular en todo lo
 * posible el comportamiento de la clase Java.util.ArrayList tal como se define
 * en la documentación javadoc de la misma, pero sin entrar en el código fuente
 * original (o sea, una estrategia de desarrollo tipo "clean room": se puede
 * analizar la documentación y los requerimientos, pero no el código fuente 
 * fuente ya existente). 
 * 
 * En esta primera versión, se asume que la clase sólo implementa Serializable,
 * y que todo otro comportamiento es simplemente una réplica de los principales
 * métodos que originalmente contiene java.util.ArrayList. 
 * 
 * @author Ing. Valerio Frittelli - Ing. Felipe Steffolani.
 * @version Agosto de 2017 - Version 1.0 (muy básica...)
 * @param <E> la clase cuyos objetos serán admisibles para la lista.
 */
public class TSBArrayList<E> implements Iterable<E>, Serializable
{
    // el arreglo que contendrá los elementos...
    private Object[] items;
    
    // el tamaño inicial del arreglo...
    private int initial_capacity;
    
    // la cantidad de casillas realmente usadas...
    private int count;

    /**
     * Crea una lista con capacidad inicial de 10 casilleros, pero ninguno
     * ocupado realmente: la lista está vacía a todos los efectos prácticos. 
     */
    public TSBArrayList()
    {
        this(10);
    }

    /**
     * Crea una lista con initialCapacity casilleros de capacidad, pero ninguno
     * ocupado realmente: la lista está vacía a todos los efectos prácticos. Si 
     * el valor de initialCapacity es <= 0, el valor se ajusta a 10.
     * @param initialCapacity la capacidad inicial de la lista.
     */
    public TSBArrayList(int initialCapacity)
    {
        if (initialCapacity <= 0) 
        {
            initialCapacity = 10;
        }
        items = new Object[initialCapacity];
        initial_capacity = initialCapacity;
        count = 0;
    }
    
    /**
     * Añade al final de la lista el objeto e. La inserción será rechazada si la
     * referencia e es null.
     * @param e el objeto a agregar en la lista.
     * @return true si la operación tuvo éxito.
     */
    public boolean add(E e)
    {
        if(e == null) return false;
        
        if(count == items.length) this.ensureCapacity(items.length * 2);
        
        items[count] = e;
        count++;
        return true;
    }

    /**
     * Añade el objeto e en la posisicón index de la lista . La inserción será 
     * rechazada si la referencia e es null (nen ese caso, el método sale sin
     * hacer nada). Si index coincide con el tamaño de la lista, el objeto e 
     * será agregado exactamente al final de la lista, como si se hubiese 
     * invocado a add(e).
     * @param index el índice de la casilla donde debe quedar el objeto e.
     * @param e el objeto a agregar en la lista.
     * @throws IndexOutOfBoundsException si index < 0 o index > size().
     */
    public void add(int index, E e)
    {        
        if(index > count || index < 0)
        {
            throw new IndexOutOfBoundsException("add(): índice fuera de rango...");
        }
        
        if(e == null) return;
        
        if(count == items.length) this.ensureCapacity(items.length * 2);
        
        int t = count - index;
        System.arraycopy(items, index, items, index+1, t);
        items[index] = e;
        count++;
    }   
    
    /**
     * Elimina todo el contenido de la lista, y reinicia su capacidad al valor
     * de la capacidad con que fue creada originalmente. La lista queda vacía 
     * luego de invocar a clear().
     */
    public void clear()
    {
        items = new Object[initial_capacity];
        count = 0;
    }
    
    /**
     * Devuelve true si la lista contiene al elemento e. Si e es null el método
     * retorna false. Puede lanzar una excepción de ClassCastException si la clase
     * de e no es compatible con el contenido de la lista.
     * @param e el objeto a buscar en la lista.
     * @return true si la lista contiene al objeto e.
     * @throws ClassCastException si e no es compatible con los objetos de la lista.
     */
    public boolean contains(Object e)
    {
        if(e == null) return false;
        
        for(int i=0; i<count; i++)
        {
            if(e.equals(items[i])) return true;
        }
        return false;
    }    
    
    /**
     * Aumenta la capacidad del arreglo de soporte, si es necesario, para 
     * asegurar que pueda contener al menos un número de elementos igual al 
     * indicado por el parámetro minCapacity.
     * @param minCapacity - la mínima capacidad requerida.
     */
    public void ensureCapacity(int minCapacity)
    {
        if(minCapacity == items.length) return;
        if(minCapacity < count) return;
        
        Object[] temp = new Object[minCapacity];
        System.arraycopy(items, 0, temp, 0, count);
        items = temp;
    }
    
    /**
     * Retorna el objeto contenido en la casilla index. Si el valor de index no 
     * es válido, el método lanzará una excepción de la clase
     * IndexOutOfBoundsException.
     * @param index índice de la casilla a acceder.
     * @return referencia al objeto contenido en la casilla index.
     * @throws IndexOutOfBoundsException si index < 0 o index >= size().
     */
    public E get(int index)
    {
        if (index < 0 || index >= count)
        {
            throw new IndexOutOfBoundsException("get(): índice fuera de rango...");
        }
        return (E) items[index];
    }
    
    /**
     * Devuelve true si la lista no contiene elementos.
     * @return true si la lista está vacía.
     */
    public boolean isEmpty()
    {
        return (count == 0);
    }   
    
    /**
     * Devuelve un iterador para la lista. 
     * @return un iterador para la lista
     */
    @Override
    public Iterator<E> iterator()
    {
        return new TSBArrayListIterator();
    }
    
    /**
     * Remueve de la lista el elemento contenido en la posición index. Los 
     * objetos ubicados a la derecha de este, se desplazan un casillero a la 
     * izquierda. El objeto removido es retornado. La capacidad de la lista no
     * se altera. Si el valor de index no es válido, el método lanzará una 
     * excepción de IndexOutOfBoundsException.
     * @param index el índice de la casilla a remover.
     * @return el objeto removido de la lista.
     * @throws IndexOutOfBoundsException si index < 0 o index >= size().
     */
    public E remove(int index)
    {
        if(index >= count || index < 0)
        {
            throw new IndexOutOfBoundsException("remove(): índice fuera de rango...");
        }
        
        int t = items.length;
        if(count < t/2) this.ensureCapacity(t/2);
        
        Object old = items[index];
        int n = count;
        System.arraycopy(items, index+1, items, index, n-index-1);
        count--;
        items[count] = null;
        return (E) old;
    }

    /**
     * Reemplaza el objeto en la posición index por el referido por element, y
     * retorna el objeto originalmente contenido en la posición index. Si el 
     * valor de index no es válido, el método lanzará una excepción de la clase
     * IndexOutOfBoundsException.
     * @param index índice de la casilla a acceder.
     * @param element el objeto que será ubicado en la posición index.
     * @return el objeto originalmente contenido en la posición index.
     * @throws IndexOutOfBoundsException si index < 0 o index >= size().
     */
    public E set(int index, E element)
    {
        if (index < 0 || index >= count)
        {
            throw new IndexOutOfBoundsException("set(): índice fuera de rango...");
        }
        Object old = items[index];
        items[index] = element;
        return (E) old;
    }

    /**
     * Retorna el tamaño de la lista: la cantidad de elementos realmente 
     * contenidos en ella.
     * @return la cantidad de elementos que la lista contiene.
     */
    public int size()
    {
        return count;
    }
       
    @Override
    public String toString()
    {
        StringBuilder buff = new StringBuilder();
        buff.append('{');
        for (int i=0; i<count; i++)
        {
            buff.append(items[i]);
            if(i < count-1)
            {
                buff.append(", ");
            }
        }
        buff.append('}');
        return buff.toString();
    }
    
    /**
     * Ajusta el tamaño del arreglo de soporte, para que coincida con el tamaño
     * de la lista. Puede usarse este método para que un programa ahorre un poco
     * de memoria en cuanto al uso de la lista, si es necesario.
     */
    public void trimToSize()
    {
        if(count == items.length) return;
        
        Object temp[] = new Object[count];
        System.arraycopy(items, 0, temp, 0, count);
        items = temp;
    }

    /**
     * Clase interna para implementar el iterador.
     */
    private class TSBArrayListIterator implements Iterator<E>
    {
        private int current;      // índice del elemento que hay que procesar.
        private boolean  next_ok; // true: next fue invocado (usado por remove()...)

        public TSBArrayListIterator()
        {
           current = -1;
           next_ok = false;
        }

       /**
        * Indica si queda algun objeto en el recorrido del iterador.          *
        * @return true si queda algun objeto en el recorrido - false si no
        * quedan objetos.
        */
       @Override
       public boolean hasNext()
       {
            if(isEmpty()) { return false; }
            if(current >= size() - 1) { return false; }
            return true;
       }

       /**
        * Retorna el siguiente objeto en el recorrido del iterador.          *
        * @return el siguiente objeto en el recorrido.
        * @throws NoSuchElementException si la lista está vacia o en la lista
        *         no quedan elementos por recorrer.
        */
       @Override
       public E next()
       {
          if (!hasNext()) { throw new NoSuchElementException("next(): no quedan elementos por recorrer..."); }

          current++;
          next_ok = true;
          return (E) items[current];
       }

       /**
        * Elimina el ultimo elemento que retornó el iterador. Debe invocarse 
        * antes de invocar a next(). El iterador queda posicionado en el 
        * elemento anterior al eliminado. 
        * @throws IllegalStateException si se invoca a remove() sin haber 
        *         invocado a next(), o si remove() fue invocado mas de una vez 
        *         luego de una sola invocacion a next().
        */
       @Override
       public void remove()
       {
          if(!next_ok) { throw new IllegalStateException("remove(): debe invocar a next() antes de remove()..."); }
        
          E garbage = TSBArrayList.this.remove(current);
          next_ok = false;
          current--;
       }
    }
}
