package clases;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ing. Valerio Frittelli.
 * @version Septiembre de 2017.
 */
public class TSBArrayListTest
{
    private TSBArrayList<Integer> instance;

    public TSBArrayListTest()
    {
    }

    @BeforeClass
    public static void setUpClass()
    {
    }

    @AfterClass
    public static void tearDownClass()
    {
    }

    @Before
    public void setUp()
    {
        System.out.println("* UtilsJUnit4Test: @Before method");
        int data[] = {1, 5, 7, 6, 10, 8, 11};

        instance = new TSBArrayList<>();
        for(int i=0; i<data.length; i++)
        {
            instance.add(i, data[i]);
        }
    }

    @After
    public void tearDown()
    {
    }

    /**
     * Test of add method, of class TSBArrayList.
     */
    @Test (expected = IndexOutOfBoundsException.class)
    public void testAdd() {
        System.out.println("add");

        // índice en rango... NO debe lanzar excepción...
        instance.add(2, 98);

        // índice fuera de rango... DEBE lanzar excepción...
        instance.add(20, 99);
    }

    /**
     * Test of contains method, of class TSBArrayList.
     */
    @Test
    public void testContains() {
        System.out.println("contains");
        assertTrue(instance.contains(5));
        assertFalse(instance.contains(99));
    }

    /**
     * Test of get method, of class TSBArrayList.
     */
    @Test
    public void testGet() {
        System.out.println("get");
        assertEquals((int)1, (int)instance.get(0));
        assertEquals((int)7, (int)instance.get(2));
        assertEquals((int)11, (int)instance.get(6));
    }

    /**
     * Test of remove method, of class TSBArrayList.
     */
    @Test (timeout=100)
    public void testRemove() {
        System.out.println("remove");
        assertEquals(11, (int)instance.remove(6));
    }
}
