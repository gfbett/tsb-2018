package edu.utn.tsb.ejemplo1;

import java.net.URL;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Ejemplo1 extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        URL urlEscena1 = getClass().getResource("Escena1.fxml");
        Parent root = FXMLLoader.load(urlEscena1);
        
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.setTitle("Primera ventana");
        stage.show();
    }
}
