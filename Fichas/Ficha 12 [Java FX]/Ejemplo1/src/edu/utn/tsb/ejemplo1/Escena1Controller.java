package edu.utn.tsb.ejemplo1;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;

public class Escena1Controller implements Initializable {

    @FXML
    private TextField txtNombre;

    @Override
    public void initialize(URL location, ResourceBundle resources) { }

    @FXML
    private void btnClick(ActionEvent event) {
        String nombre = txtNombre.getText();
        String saludo = "Bienvenido " + nombre + "!";
        
        Alert dialogo = new Alert(Alert.AlertType.INFORMATION);
        dialogo.setTitle("Saludo");
        dialogo.setHeaderText(saludo);
        dialogo.setContentText("Mensaje de bienvenida");
        dialogo.showAndWait();
    }
}
