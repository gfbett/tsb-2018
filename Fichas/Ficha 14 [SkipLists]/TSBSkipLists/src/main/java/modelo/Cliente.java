package modelo;

/**
 * Clase sencilla, usada para representar Clientes del Banco. 
 * 
 * @author Ing. Valerio Frittelli.
 * @version Mayo de 2004.
 */
public class Cliente implements Comparable<Cliente>
{
    private int dni;
    private String nombre;
    
    /**
     * Constructor por defecto.  
     */
    public Cliente()
    {
    }

    /**
     * Constructor sobrecargado. Inicializa el dni y el nombre de acuerdo a los 
     * parámetros.
     * @param d el número de dni del cliente.
     * @param nom el nombre del cliente.
     */
    public Cliente(int d, String nom)
    {
      dni = d;
      nombre = nom;
    }
    
    /*
     *  Metodo de consulta para el numero de dni.
     *  @return el numero de dni.
     */
    public int getDni()
    {
       return dni;   
    }

    /*
     *  Metodo de consulta para el nombre del cliente.
     *  @return el nombre del cliente.
     */
    public String getNombre()
    {
       return nombre;   
    }
    
    /*
     *  Metodo modificador para el numero de dni.
     *  @param num el nuevo dni.
     */
    public void setDni( int num )
    {
       dni = num;   
    }

    /*
     *  Metodo modificador para el nombre del cliente.
     *  @param nom el nuevo nombre.
     */
    public void setNombre( String nom )
    {
       nombre = nom;   
    }

    /** 
     *  Redefinicion del metodo toString.
     *  @return el contenido del objeto en forma String con formato adecuado 
     *          para ser visualizado.
     */
    @Override
    public String toString()
    {
       return "Dni del cliente: " + dni +  " - Nombre: " + nombre;
    }

    @Override
    public int compareTo(Cliente x)
    {
       return this.getDni() - x.getDni(); 
    }
}
