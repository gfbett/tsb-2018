package modelo;

public class Persona implements Comparable<Persona>
{
  private int dni;
  private String nombre;
  private int edad;

  public Persona(int doc)
  {
     dni = doc;
  }
  
  public Persona(int doc, String nom, int e)
  {
     dni = doc;
     nombre = nom;
     edad = e;
  }
  
  @Override
  public String toString()
  {
     return "Dni: " + dni +  " - Nombre: " + nombre + " - Edad: " + edad;
  }


  @Override
  public int compareTo(Persona x)
  {
     return this.dni - x.dni; 
  }
}