

public class HolaMundo {

    public static void main(String[] args) {

        int a = 10;
        int b = 32;
        int suma = a + b;
        int resta = a - b;
        int multiplicacion = a * b;
        double division = a / (double)b;

        System.out.println("Suma: " + suma);
        System.out.println("Resta: " + resta);
        System.out.println("Multiplicacion: " + multiplicacion);
        System.out.println("Division: " + division);

        Persona p = new Persona();
        p.setDni(123);
        Persona q;
        q = p;
        q.setDni(234);
        System.out.println("Dni p:" + p.getDni());
        System.out.println("Dni q:" + q.getDni());
        System.gc();
    }

}
