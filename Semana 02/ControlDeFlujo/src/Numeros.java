import java.util.Scanner;

public class Numeros {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese la cantidad de números: ");
        int n = sc.nextInt();

        int[] v = new int[n];
        int acumulador = 0;
        for (int i = 0; i < v.length; i++) {
            System.out.println("Ingrese un valor: ");
            v[i] = sc.nextInt();
            acumulador += v[i];
        }

        double promedio = (double)acumulador / n;
        int cantidad = 0;
        for (int x : v) {
            if (x > promedio) {
                cantidad++;
            }
        }

        System.out.println("La cantidad de mayores al promedio es: " + cantidad);

    }
}
