import java.util.Scanner;

public class Principal {
    public static void main(String[] args) {

        int[] v = {1, 2, 3};
        int [][] m = {{1}, {2, 3}, {4, 5, 6}};
        for (int i = 0; i < m.length; i++) {
            for (int j = 0; j < m[i].length; j++) {
                System.out.println(m[i][j]);
            }
        }
    }
}
