public class Alumno extends Persona {

    private int legajo;

    public Alumno() {
        this(0, "", 0);
    }

    public Alumno(int dni, String nombre, int legajo) {
        super(dni, nombre);
        System.out.println("Hola");
        this.legajo = legajo;
    }

    public int getLegajo() {
        return legajo;
    }

    public void setLegajo(int legajo) {
        this.legajo = legajo;
    }

    @Override
    public void mostrar() {
        System.out.println("Alumno");
    }
}
