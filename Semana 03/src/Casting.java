public class Casting {
    public static void main(String[] args) {
        int a = 35000;
        short b = (short)a;
        System.out.println(b);
        byte c = 127;
        c++;
        System.out.println(c);
        char cc = 'X';
        int d = cc;
        System.out.println(d);
        float f = 3.14f;
        int l = 0100;
        System.out.println(l);
    }
}
