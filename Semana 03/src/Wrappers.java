public class Wrappers {
    public static void main(String[] args) {
        String x = "3.141592654";
        float nro = Float.parseFloat(x);
        Float objeto = nro;
        System.out.println(nro);
        System.out.println(objeto);
    }

    /**
     * Función que suma dos números
     * @param a Un número
     * @param b Otro Número
     * @return La suma
     */
    static int sumar(int a, int b) {
        return a + b;
    }
}
