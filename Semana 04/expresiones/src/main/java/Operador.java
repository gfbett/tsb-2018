public class Operador implements Calculable {
    private char operacion;
    private Calculable izquierdo;
    private Calculable derecho;

    public Operador(char operacion, Calculable izquierdo, Calculable derecho) {
        this.operacion = operacion;
        this.izquierdo = izquierdo;
        this.derecho = derecho;
    }

    public int calcular() {
        int izq = izquierdo.calcular();
        int der = derecho.calcular();
        switch(operacion) {
            case '+':
                return izq + der;
            case '-':
                return izq - der;
            case '*':
                return izq * der;
            case '/':
                return izq / der;
        }
        return -1;
    }

    @Override
    public String toString() {
        return izquierdo.toString() + " " + operacion +
                " " + derecho.toString();
    }
}
