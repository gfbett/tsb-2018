public class Operando implements Calculable{
    private int value;

    public Operando(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public int calcular() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }
}
