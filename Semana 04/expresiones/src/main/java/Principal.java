public class Principal {

    public static void main(String[] args) {

        Operando o1 = new Operando(3);
        Operando o2 = new Operando(2);
        Operando o3 = new Operando(5);
        Operador mul1 = new Operador('*', o2, o3);
        Operador sum1 = new Operador('+', o1, mul1);

        System.out.println(sum1);
        System.out.println("Resultado: " + sum1.calcular());

    }
}
