public class Auto {
    private String patente;
    private String marca;
    private int modelo;

    public Auto(String patente, String marca, int modelo) {
        this.patente = patente;
        this.marca = marca;
        this.modelo = modelo;
    }

    public String getPatente() {
        return patente;
    }

    public String getMarca() {
        return marca;
    }

    public int getModelo() {
        return modelo;
    }

    public int antiguedad() {
        return 2018 - modelo;
    }

    public double impuestos() {
        int antiguedad = antiguedad();
        if (antiguedad < 10) {
            return 200;
        } else if (antiguedad < 20) {
            return 150;
        } else {
            return 0;
        }
    }

    @Override
    public String toString() {
        return "Auto{" +
                "patente='" + patente +
                ", marca='" + marca +
                ", modelo=" + modelo +
                "}\n";
    }
}
