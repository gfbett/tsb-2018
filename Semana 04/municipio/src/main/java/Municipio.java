public class Municipio {
    private Auto[] vehiculos;

    public Municipio(int tamanio) {
        vehiculos = new Auto[tamanio];
    }

    public void setVehiculo(Auto a, int ix) {
        vehiculos[ix] = a;
    }

    public String listar() {
        StringBuilder sb = new StringBuilder();
        for (Auto auto: vehiculos) {
            sb.append(auto.toString());
        }
        return sb.toString();
    }

    public void impuestos() {
        for (Auto auto:vehiculos) {
            System.out.println(auto.impuestos());
        }
    }
}
