import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Principal {
    static Scanner sc;

    public static void main(String[] args) {
        try {
            sc = new Scanner(new File("datos.txt"));
        } catch (FileNotFoundException e) {
            System.out.println("No se encontró el archivo.");
        }

        System.out.print("Ingrese la cantidad de vehiculos: ");
        int n = sc.nextInt();

        Municipio municipio = new Municipio(n);

        cargarVehiculos(municipio, n);

        System.out.println(municipio.listar());
        municipio.impuestos();

    }

    private static void cargarVehiculos(Municipio municipio, int n) {
        for (int i = 0; i < n; i++) {
            Auto auto = cargarAuto();
            municipio.setVehiculo(auto, i);
        }
    }

    private static Auto cargarAuto() {
        System.out.println("Ingrese tipo de vehiculo (1 auto, 2 taxi, 3 remis): ");
        int licencia = 0, agencia = 0;
        int tipo = sc.nextInt();
        System.out.println("Ingrese patente: ");
        String patente = sc.next();
        System.out.println("Ingrese marca: ");
        String marca = sc.next();
        System.out.println("Ingrese modelo: ");
        int modelo = sc.nextInt();
        if (tipo > 1) {
            System.out.println("Ingrese licencia: ");
            licencia = sc.nextInt();
            if (tipo > 2) {
                System.out.println("Ingrese agencia: ");
                agencia = sc.nextInt();
            }
        }


        switch (tipo) {
            case 1:
                return new Auto(patente, marca, modelo);
            case 2:
                return new Taxi(patente, marca, modelo, licencia);
            case 3:
                return new Remis(patente, marca, modelo, licencia, agencia);
        }
        return null;
    }
}
