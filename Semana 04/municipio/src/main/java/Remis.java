public class Remis extends Auto{
    private int licencia;
    private int agencia;

    public Remis(String patente, String marca, int modelo, int licencia, int agencia) {
        super(patente, marca, modelo);
        this.licencia = licencia;
        this.agencia = agencia;
    }

    public int getLicencia() {
        return licencia;
    }

    public int getAgencia() {
        return agencia;
    }

    @Override
    public double impuestos() {
        return super.impuestos() * 1.1;
    }

    @Override
    public String toString() {
        return "Remis{" +
                "patente='" + getPatente() +
                ", marca='" + getMarca() +
                ", modelo=" + getModelo() +
                ", licencia=" + licencia +
                ", agencia=" + agencia +
                "}\n";
    }
}
