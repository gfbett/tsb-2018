public class Taxi extends Auto {
    private int licencia;


    public Taxi(String patente, String marca, int modelo, int licencia) {
        super(patente, marca, modelo);
        this.licencia = licencia;
    }

    public int getLicencia() {
        return licencia;
    }

    @Override
    public double impuestos() {
        return super.impuestos() + 1500;
    }

    @Override
    public String toString() {
        return "Taxi{" +
                "patente='" + getPatente() +
                ", marca='" + getMarca() +
                ", modelo=" + getModelo() +
                ", licencia=" + licencia +
                "}\n";
    }
}
