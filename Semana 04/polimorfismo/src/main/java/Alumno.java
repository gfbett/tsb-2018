public class Alumno extends Persona {
    int legajo;

    @Override
    public String toString() {
        return "Alumno{" +
                "legajo=" + legajo +
                ", dni=" + dni +
                ", nombre='" + nombre + '\'' +
                '}';
    }

    public void mostrar() {
        System.out.println("Alumno!!!");
    }

    public String toJSON() {
        return null;
    }
}
