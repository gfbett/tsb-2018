public class Docente extends Persona {
    String curso;

    @Override
    public String toString() {
        return "Docente{" +
                "curso='" + curso + '\'' +
                ", dni=" + dni +
                ", nombre='" + nombre + '\'' +
                '}';
    }

    public void mostrar() {
        System.out.println("Docente!!!");
    }
}
