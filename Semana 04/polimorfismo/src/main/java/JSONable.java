public interface JSONable {

    String toJSON();
}
