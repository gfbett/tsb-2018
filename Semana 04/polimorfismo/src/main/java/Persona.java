public abstract class Persona implements JSONable, Mostrable {
    int dni;
    String nombre;

    @Override
    public String toString() {
        return "Persona{" +
                "dni=" + dni +
                ", nombre='" + nombre + '\'' +
                '}';
    }

    public String toJSON() {
        return null;
    }
}
