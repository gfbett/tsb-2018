public class Principal {
    public static void main(String[] args) {

        Persona p = new Alumno();
        if (p instanceof Alumno) {
            Alumno x = (Alumno) p;
            x.legajo = 432;
        }
        p.nombre = "Juan Perez";
        p.dni = 12345;

        System.out.println(p);
        p.mostrar();

        Integer i1 = new Integer(42);
        Integer i2 = new Integer(42);
        if (i1.getClass() == i2.getClass()) {

        }
        if (i1.equals(i2)) {
            System.out.println("Iguales");
        } else {
            System.out.println("Distintos");
        }
    }
}
