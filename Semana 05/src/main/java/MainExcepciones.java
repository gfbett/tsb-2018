import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

public class MainExcepciones {

    public static void main(String[] args) {
        System.out.println("main - antes");
        try {
            f1();
        } catch (Exception e) {
            System.out.println("ERROR!");
            e.printStackTrace();
        }
        System.out.println("main - despues");
    }

    static void f1() {
        System.out.println("f1 - antes");
        f2();
        System.out.println("f1 - despues");
    }
    static void f2() {
        System.out.println("f2 - antes");
        f3();
        System.out.println("f2 - despues");
    }
    static void f3() {
        System.out.println("f3 - antes");
        try {
            f4();
        } catch (NullPointerException | ArrayIndexOutOfBoundsException e) {
            System.err.println("ERRRORRRRRRR!!!");
            Integer x = null;
            x.compareTo(2);
        } catch (Exception ex) {
            System.err.println("ERROR!!!" + ex.getMessage());
            throw new RuntimeException("Error", ex);
        }
        System.out.println("f3 - despues");
    }
    static void f4() throws Exception {
        System.out.println("f4 - antes");
        f5();
        System.out.println("f4 - despues");
    }
    static void f5() throws Exception {
        try (RandomAccessFile f = new RandomAccessFile("datos.txt", "r")) {
            f.readLine();
        }
        throw new Exception();
    }
}
