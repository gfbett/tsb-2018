public class Principal_Static {

    public static final int LA_RESPUESTA;

    static {
        LA_RESPUESTA = 42;
    }

    public static void main(String[] args) {

        Factura.secuencia = LA_RESPUESTA;
        Factura f1 = Factura.generarFactura("Juan");
        Factura f2 = Factura.generarFactura("Laura");
        Factura f3 = Factura.generarFactura("Pepe");
        System.out.println(f1);
        System.out.println(f2);
        System.out.println(f3);
        final int a = 3;


    }
}

class Factura {
    static int secuencia;
    final int nro;
    String nombreDestinatario;

    public static Factura generarFactura(String dest) {
        return new Factura(++secuencia, dest);
    }


    private Factura(final int nro, String nombreDestinatario) {
        this.nombreDestinatario = nombreDestinatario;
        this.nro = nro;
    }


    @Override
    public String toString() {
        return "Factura{" +
                "nro=" + nro +
                ", nombreDestinatario='" + nombreDestinatario + '\'' +
                '}';
    }
}