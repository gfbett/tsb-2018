import java.util.NoSuchElementException;

public class SimpleList {

    private Nodo frente;
    private int size;

    public SimpleList() {
        frente = null;
        size = 0;
    }

    public void addFirst(Object x){
        Nodo nuevo = new Nodo(x);
        nuevo.next = frente;
        frente = nuevo;
        size ++;
    }

    public Object removeFirst() {
        if (size == 0) {
            throw new NoSuchElementException();
        }
        Object valor = frente.info;
        frente = frente.next;
        size --;
        return valor;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("[");
        Nodo p = frente;
        while(p != null) {
            sb.append(p.info);
            sb.append(" - ");
            p = p.next;
        }
        sb.append("]");
        return sb.toString();
    }
}

class Nodo {
    Object info;
    Nodo next;

    public Nodo(Object info) {
        this.info = info;
    }
}