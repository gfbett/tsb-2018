import java.util.Iterator;
import java.util.NoSuchElementException;

public class SimpleList <T extends Comparable> implements Iterable {

    private Node<T> root;
    private int size;

    public int getSize() {
        return size;
    }

    public SimpleList() {
        root = null;
        size = 0;
    }

    public Iterator iterator() {
        return new SimpleListIterator();
    }


    public void addFirst(T x) {
        Node<T> n = new Node<T>(x);
        n.next = root;
        root = n;
        size ++;
    }

    public T get(int index) {
        if (index < 0 || index >= size) {
            throw new NoSuchElementException("No such index");
        }
        Node<T> p = root;
        for (int i = 0; i < index; i++) {
            p = p.next;
        }
        return p.info;
    }



    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("[");
        Node<T> p = root;
        while(p != null) {
            sb.append(p.info);
            sb.append(", ");
            p = p.next;
        }
        sb.append("]");
        return sb.toString();
    }



    private class SimpleListIterator<T> implements Iterator<T> {

        private Node<T> actual;

        public SimpleListIterator() {
            actual = (Node<T>) root;
        }

        public boolean hasNext() {
            return actual != null;
        }

        public T next() {
            T value = actual.info;
            actual = actual.next;
            return value;
        }

        public void remove() {

        }
    }

    private static class Node <T> {
        T info;
        Node<T> next;

        Node(T info) {
            this.info = info;
        }
    }

}

