public class StringSimpleList {
    private SimpleList list;

    public StringSimpleList() {
        this.list = new SimpleList();
    }

    void addFirst(String x) {
        list.addFirst(x);
    }

    String get(int index) {
        return (String)list.get(index);
    }

    int getSize() {
        return list.getSize();
    }

    public String toString() {
        return list.toString();
    }
}
