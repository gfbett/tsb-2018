import java.io.Serializable;

class Persona implements Serializable {
    int dni;
    String nombre;
    transient int edad;

    private static final long serialVersionUID = 1;

    @Override
    public String toString() {
        return "Persona{" +
                "dni=" + dni +
                ", nombre='" + nombre + '\'' +
                ", edad=" + edad +
                '}';
    }

    public Persona(int dni, String nombre, int edad) {
        this.dni = dni;
        this.nombre = nombre;
        this.edad = edad;
    }

}
