import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Iterator;

public class Principal {
    public static void main(String[] args) {

        TSBArrayList<Integer> list = new TSBArrayList<>();
        if (list.size() != 0) {
            throw new RuntimeException("Size no esperado");
        }
        if (!list.isEmpty()) throw new RuntimeException("isEmpty mal");
        list.add(1);
        list.add(2);
        if (list.isEmpty()) throw new RuntimeException("isEmpty mal");
        if (list.size() != 2) {
            throw new RuntimeException("Size no esperado");
        }
        if (!list.toString().equals("[1, 2]")) {
            throw new RuntimeException("toString mal");
        }
        for (int i = 0; i < 10; i++) {
            list.add(i);
        }
        if (!list.toString().equals("[1, 2, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9]")) {
            throw new RuntimeException("toString mal");
        }

        try {
            list.get(-1);
        } catch (IndexOutOfBoundsException e) {
            System.out.println("get -1 ok");
        }

        try {
            list.get(list.size() + 1 );
        } catch (IndexOutOfBoundsException e) {
            System.out.println("get size + 1 ok");
        }

        if (list.get(0) != 1) {
            throw new RuntimeException("Error");
        }
        if (list.get(11) != 9) {
            throw new RuntimeException("Error");
        }
        if (list.get(6) != 4) {
            throw new RuntimeException("Error");
        }

        if (list.contains(42)) throw new RuntimeException("contains");
        if (!list.contains(3)) throw new RuntimeException("contains");


        list.clear();
        if (!list.toString().equals("[]")) throw new RuntimeException("clear");
        if (!list.isEmpty()) throw new RuntimeException("clear");
        if (list.size() != 0) throw new RuntimeException("clear");


        for (int i = 0; i < 5; i++) {
            list.add(i);
        }

        if (list.set(2, 6) != 2) throw new RuntimeException("set");
        if (!list.toString().equals("[0, 1, 6, 3, 4]")) throw new RuntimeException("set");
        if (list.get(2) != 6) throw new RuntimeException("set");

        list.clear();
        for (int i = 0; i < 5; i++) {
            list.add(i);
        }
        list.add(1, 5);
        if (!list.toString().equals("[0, 5, 1, 2, 3, 4]")) throw new RuntimeException("add");
        list.add(list.size(), 7);
        if (!list.toString().equals("[0, 5, 1, 2, 3, 4, 7]")) throw new RuntimeException("add");
        list.add(0, 8);
        if (!list.toString().equals("[8, 0, 5, 1, 2, 3, 4, 7]")) throw new RuntimeException("add");
        System.out.println(list);


        int x = list.remove(0);
        if (x != 8) throw new RuntimeException("remove");
        if (list.size() != 7) throw new RuntimeException("remove");
        if (!list.toString().equals("[0, 5, 1, 2, 3, 4, 7]")) throw new RuntimeException("remove");

        x = list.remove(list.size() - 1);
        if (x != 7) throw new RuntimeException("remove");
        if (list.size() != 6) throw new RuntimeException("remove");
        if (!list.toString().equals("[0, 5, 1, 2, 3, 4]")) throw new RuntimeException("remove");

        x = list.remove(3);
        if (x != 2) throw new RuntimeException("remove");
        if (list.size() != 5) throw new RuntimeException("remove");
        if (!list.toString().equals("[0, 5, 1, 3, 4]")) throw new RuntimeException("remove");

        list = new TSBArrayList<>();
        Iterator<Integer> it = list.iterator();
        if (it.hasNext()) throw new RuntimeException("hasNext");
        list.add(1);
        if (!it.hasNext()) throw new RuntimeException("hasNext");

        list.clear();
        for (int i = 10; i >= 0; i--) {
            list.add(i);
        }
        int c = 10;
        for(Integer num: list) {
            if (num != c) throw new RuntimeException("Iterator");
            c--;
        }

        list.clear();
        for (int i = 0; i < 10; i++) {
            list.add(i);
        }
        it = list.iterator();
        for (int i = 0; i < 4; i++) {
            it.hasNext();
            it.next();
        }
        it.remove();
        if (!it.hasNext()) throw new RuntimeException("remove");
        int next = it.next();
        System.out.println(next);
        if (next != 4) throw new RuntimeException("remove");


        Wrapper p = new Wrapper(42);
        TSBArrayList<Wrapper> list2 = new TSBArrayList<>();
        list2.add(p);

        try {
            FileOutputStream fos = new FileOutputStream("datos.dat");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(list);
            oos.writeObject(list2);

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private static class Wrapper implements Serializable {
        int i;

        public Wrapper(int i) {
            this.i = i;
        }
    }
}
