import java.io.*;

public class ReadSerialization {
    public static void main(String[] args) {

        Persona p;

        try {
            FileInputStream fis = new FileInputStream("datos.dat");
            ObjectInputStream ois = new ObjectInputStream(fis);
            p = (Persona)ois.readObject();
            System.out.println(p);

        } catch (IOException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }
    }
}
