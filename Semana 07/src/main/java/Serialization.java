import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class Serialization {
    public static void main(String[] args) {

        Persona p = new Persona(123, "Homero", 42);

        try {
            FileOutputStream fos = new FileOutputStream("datos.dat");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(p);

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}

