import java.io.Serializable;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class TSBArrayList<E> implements Iterable<E>, Serializable {

    private E[] v;
    private int size;

    public TSBArrayList() {
        v = (E[]) new Object[10];
        size = 0;
    }

    void add(E x) {
        ensureCapacity();
        v[size] = x;
        size++;
    }

    private void ensureCapacity() {
        if (size == v.length) {
            Object[] aux = new Object[size * 2];
            System.arraycopy(v, 0, aux, 0, size);
            v = (E[]) aux;
        }
    }

    int size() {
        return size;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("[");
        for (int i = 0; i < size; i++) {
            sb.append(v[i]);
            if (i < size - 1) {
                sb.append(", ");
            }
        }
        sb.append("]");
        return sb.toString();
    }

    boolean isEmpty() {
        return size == 0;
    }

    E get(int i) {
        validateInSize(i);
        return v[i];
    }


    void clear() {
        v = (E[]) new Object[10];
        size = 0;
    }

    boolean contains(E x) {
        int i = 0;
        for (; i < size; i++) {
            if (v[i].equals(x)) {
                break;
            }
        }
        return i != size;
    }

    E set(int i, E x) {
        validateInSize(i);
        E aux = v[i];
        v[i] = x;
        return aux;
    }

    private void validateInSize(int i) {
        if (i < 0 || i >= size) {
            throw new IndexOutOfBoundsException();
        }
    }


    void add(int i, E x) {
        if (i < 0 || i > size) {
            throw new IndexOutOfBoundsException();
        }
        ensureCapacity();
        //Hacer lugar
        if (i != size) {
            System.arraycopy(v, i, v, i+1, size - i);
        }
        v[i] = x;
        size ++;

    }

    E remove(int i) {
        validateInSize(i);
        E aux = v[i];
        System.arraycopy(v, i+1, v, i , size-i-1);
        size--;
        return aux;
    }

    @Override
    public Iterator<E> iterator() {
        return new TSBArrayListIterator();
    }

    private class TSBArrayListIterator implements Iterator<E> {

        private int actual;
        private int lastNext;

        public TSBArrayListIterator() {
            actual = 0;
            lastNext = -1;
        }

        @Override
        public boolean hasNext() {
            return actual < size;
        }

        @Override
        public E next() {
            if (!hasNext()) { throw new NoSuchElementException(); }
            E aux = v[actual];
            lastNext = actual;
            actual++;
            return aux;
        }

        @Override
        public void remove() {
            if (lastNext == -1) {
                throw new IllegalStateException();
            }
            TSBArrayList.this.remove(lastNext);
            lastNext = -1;
            actual--;

        }
    }
}
