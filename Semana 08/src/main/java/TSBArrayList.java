import java.io.Serializable;
import java.util.AbstractList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public class TSBArrayList<E> extends AbstractList<E> implements Cloneable, Iterable<E>, Serializable {

    private E[] v;
    private int size;

    public TSBArrayList() {
        v = (E[]) new Object[10];
        size = 0;
    }


    public TSBArrayList clone() {
        TSBArrayList<E> newList = new TSBArrayList();
        newList.v = (E[]) new Object[v.length];
        System.arraycopy(v, 0, newList.v, 0,size);
        newList.size = size;
        return newList;
    }

    private void ensureCapacity() {
        if (size == v.length) {
            Object[] aux = new Object[size * 2];
            System.arraycopy(v, 0, aux, 0, size);
            v = (E[]) aux;
        }
    }

    public int size() {
        return size;
    }


    public E get(int i) {
        validateInSize(i);
        return v[i];
    }


    public void clear() {
        v = (E[]) new Object[10];
        size = 0;
        modCount++;
    }

    public E set(int i, E x) {
        validateInSize(i);
        E aux = v[i];
        v[i] = x;
        modCount++;
        return aux;
    }

    private void validateInSize(int i) {
        if (i < 0 || i >= size) {
            throw new IndexOutOfBoundsException();
        }
    }


    public void add(int i, E x) {
        if (i < 0 || i > size) {
            throw new IndexOutOfBoundsException();
        }
        ensureCapacity();
        //Hacer lugar
        if (i != size) {
            System.arraycopy(v, i, v, i+1, size - i);
        }
        v[i] = x;
        size ++;
        modCount++;
    }

    public E remove(int i) {
        validateInSize(i);
        E aux = v[i];
        System.arraycopy(v, i+1, v, i , size-i-1);
        size--;
        modCount++;
        return aux;
    }

    @Override
    public String toString() {
        return "TSBArrayList{" +
                "size=" + size +
                '}';
    }
}
