import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TSBArrayListTest {

    private TSBArrayList<Integer> list;

    @Before
    public void setUp() {
        list = new TSBArrayList<>();
    }

    @Test
    public void testSizeEmptyList() {
        assertEquals(0, list.size());
    }

    @Test
    public void testSizeNonEmptyList() {
        list.add(1);
        assertEquals(1, list.size());
        list.add(2);
        assertEquals(2, list.size());
    }

    @Test
    public void testIsEmptyEmptyList() {
        assertTrue(list.isEmpty());
    }

    @Test
    public void testIsEmpty() {
        list.add(1);
        assertFalse(list.isEmpty());
        list.remove(0);
        assertTrue(list.isEmpty());
    }

    @Test
    public void testGet() {
        for (int i = 0; i < 10; i++) {
            list.add(i);
        }
        assertEquals(Integer.valueOf(3), list.get(3));
        assertEquals(Integer.valueOf(0), list.get(0));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testGetNegative() {
        list.get(-1);
    }

    @Test
    public void testCloneNotNull() {
        list.add(1);
        TSBArrayList cloned = list.clone();
        assertNotNull(cloned);
    }
    @Test
    public void testCloneEquals() {
        list.add(1);
        TSBArrayList cloned = list.clone();
        assertEquals(list, cloned);
    }
    @Test
    public void testCloneNotSame() {
        list.add(1);
        TSBArrayList cloned = list.clone();
        list.set(0, 2);
        assertNotEquals(list.get(0), cloned.get(0));
    }

    @Test
    public void testToStringEmpty() {
        assertEquals("[]", list.toString());
    }


}