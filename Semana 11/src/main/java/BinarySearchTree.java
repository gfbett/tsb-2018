public class BinarySearchTree <E extends Comparable> {

    private TreeNode<E> root;
    private int size;

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public boolean add(E x) {
        TreeNode<E> q = null;
        TreeNode<E> p = root;
        while (p != null && p.info != x) {
            q = p;
            if (x.compareTo(p.info) < 0) {
                p = p.left;
            } else {
                p = p.right;
            }
        }
        if (p == null) {
            TreeNode<E> n = new TreeNode<>(x);
            if (q == null) {
                root = n;
            } else if (x.compareTo(q.info) < 0) {
                q.left = n;
            } else {
                q.right = n;
            }
            size ++;
            return true;
        }

        return false;
    }

    public boolean contains(E x) {
        TreeNode<E> p = root;
        while (p != null && p.info != x) {
            if (x.compareTo(p.info) < 0) {
                p = p.left;
            } else {
                p = p.right;
            }
        }
        return p != null;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        print(sb, root);
        sb.append("]");
        return sb.toString();
    }

    private void print(StringBuilder sb, TreeNode<E> p) {
        if (p != null) {
            print(sb, p.left);
            sb.append(p.info);
            sb.append(", ");
            print(sb, p.right);
        }
    }


    private static class TreeNode<E extends Comparable> {
        E info;
        TreeNode<E> left;
        TreeNode<E> right;

        TreeNode(E info) {
            this.info = info;
        }

        @Override
        public String toString() {
            return info.toString();
        }
    }
}
