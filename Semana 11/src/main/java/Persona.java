import java.util.Comparator;

public class Persona implements Comparable<Persona>{
    private int dni;
    private String nombre;
    private String apellido;

    public Persona(int dni, String nombre, String apellido) {
        this.dni = dni;
        this.nombre = nombre;
        this.apellido = apellido;
    }

    public int getDni() {
        return dni;
    }

    public void setDni(int dni) {
        this.dni = dni;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public int compareTo(Persona persona) {
        return this.dni - persona.dni;
    }

    public class NombreComparator implements Comparator<Persona> {

        public int compare(Persona p1, Persona p2) {
            return p1.nombre.compareTo(p2.nombre);
        }
    }
}
