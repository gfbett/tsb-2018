import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class BinarySearchTreeTest {

    private BinarySearchTree<Integer> tree;

    @Before
    public void setup() {
        tree = new BinarySearchTree<Integer>();
    }

    @Test
    public void testEmptySize() {
        assertEquals(0, tree.size());
    }

    @Test
    public void testEmpty() {
        assertTrue(tree.isEmpty());
    }

    @Test
    public void testAdd() {
        assertTrue(tree.add(1));
        assertEquals(1, tree.size());
        assertFalse(tree.isEmpty());
    }

    @Test
    public void testAddRepeated() {
        assertTrue(tree.add(1));
        assertTrue(tree.add(2));
        assertFalse(tree.add(1));
    }

    @Test
    public void testContains() {
        tree.add(1);
        tree.add(3);
        tree.add(2);
        assertTrue(tree.contains(1));
        assertTrue(tree.contains(3));
        assertFalse(tree.contains(4));
    }

    @Test
    public void TestToString() {
        int[] seq = {5, 4, 9, 1, 10, 6, 3, 7, 8, 2};
        for (Integer x : seq) {
            tree.add(x);
        }
        assertEquals("[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, ]", tree.toString());
    }
}
