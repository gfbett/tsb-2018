public class SkipList {
    private static final int MAX_LEVEL = 16;

    private int size;
    private int level;
    private Node root;

    public SkipList() {
        root = new Node(-1, MAX_LEVEL);
        level = 0;
        size = 0;
    }

    public int size() {
        return size;
    }

    void add(int x) {
        Node p = root;
        Node[] update = new Node[MAX_LEVEL];
        for(int i = level; i >= 0; i--) {
            while (p.next[i] != null && p.next[i].info < x) {
                p = p.next[i];
            }
            update[i] = p;
        }
        p = p.next[0];
        int newLevel = newLevel();
        Node n = new Node(x, newLevel);
        for (int i = 0; i <= newLevel; i++) {
            if (update[i] != null) {
                n.next[i] = update[i].next[i];
                update[i].next[i] = n;
            } else {
                root.next[i] = n;
            }
        }
        if (newLevel > level) {
            level = newLevel;
        }
        size ++;
    }

    private int newLevel() {
        int newLevel = 0;
        while (Math.random() < 0.5) {
            newLevel++;
        }
        if (newLevel > level) {
            newLevel = level + 1;
        }
        return newLevel > MAX_LEVEL? MAX_LEVEL: newLevel;
    }

    boolean contains(int x) {
        Node p = root;
        for(int i = level; i >= 0; i--) {
            while (p.next[i] != null && p.next[i].info < x) {
                p = p.next[i];
            }
        }
        p = p.next[0];
        return p != null && p.info == x;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        Node p = root;
        while (p != null) {
            sb.append(p.info);
            sb.append(":(");
            for (int i = 0; i < p.next.length; i++) {
                sb.append(p.next[i] != null ? p.next[i].info: "n");
                sb.append(", ");
            }

            sb.append(")\n");
            p = p.next[0];
        }
        return sb.toString();
    }

    private static class Node {
        int info;
        Node[] next;

        public Node(int info, int level) {
            this.info = info;
            next = new Node[level + 1];
        }
    }
}
