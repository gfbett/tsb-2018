import org.junit.Before;
import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.*;

public class SkipListTest {

    private SkipList list;

    @Before
    public void setUp() {
        list = new SkipList();

    }


    @Test
    public void testInsert() {
        for (int i = 9; i >= 0; i--) {
            System.out.println("Adding:" + i);
            list.add(i);
            System.out.println(list);
        }
        assertEquals(10, list.size());
        System.out.println(list);
        for (int i = 0; i < 10; i++) {
            assertTrue(list.contains(i));
        }
    }

}